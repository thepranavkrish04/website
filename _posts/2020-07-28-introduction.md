---
layout: post
author: tourpran
title: Introduction to Assembly Programming - 00
categories: YouTube
tags: [YouTube]
---

<h3>Description:</h3>
This is the First video in a series called [binary Exploitation and Reverse Engineering](https://www.youtube.com/watch?v=R-PxxdB7ISo&list=PL7cHg6AXcMXDaS3A8dk1e8uGtpdv59at8&ab_channel=tourpran). Here I talked about the Essence of Assembly Programming.
![video1](https://youtu.be/R-PxxdB7ISo)